# Hangman

Simple Hangman Game in Python. 

It includes a text file with all of the words. 

Please make sure to keep all the files in one directory or else it will not work. 

This game selects a random word from the file and asks you to guess the word. 

You will get 8 chances to guess a word, when you guess a correct character the number of chances will not be reduced by one.